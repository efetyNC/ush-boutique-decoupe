jQuery(document).ready(function ($) {

    /*** VARIABLES GENERALES ***/

    var $window = $(window);
    var $document = $(document);
    var header = $('header');
    var megamenu = $('.megamenu');
    var burger = $('.burger');
    var body = $('body');
    var footer = $('footer');
    var niveau1_li = $('.menu-desktop > li');
    var niveau1_a = $('.menu-desktop > li > a');
    var burger_ul = $('.menu-mobile ul');
    var burger_ul_3e_nv = $('.menu-mobile ul ul');
    var burger_header = $('.burger-header');

    /*** FIN VARIABLES GENERALES ***/

    /*** VARIABLE DE NAVIGATEUR ***/
    const useragent = window.navigator.userAgent;
//Test IE 11
    if(useragent.indexOf("Trident/7.0") > 0){
        var ie = true
    }
// Test Safari
    if (useragent.indexOf("Safari") > 0 && useragent.indexOf("Macintosh") > 0 && useragent.indexOf("Chrome") < 1){
        var safari = true;
    }
    /*** FIN VAR NAVIGATEUR ***/

    //ANIMATIONS

    if ($(window).width() > 991){

        //Initialisation des animations
        $('.load-fade, .load-flip, .load-zoom, .load-swipe-l, .load-swipe-r, .load-swipe-t, .load-swipe-b').addClass('-init');

        $(window).scroll(function () {

            //Suppression de la class '--init' afin d'effectuer l'animation
            $('.load-fade, .load-flip, .load-zoom, .load-swipe-l, .load-swipe-r, .load-swipe-t, .load-swipe-b').each(function (e) {
                if ($(this).hasClass('load-swipe-b')){
                    var bottom_of_object = $(this).offset().top + $(this).outerHeight() - 500; //Gestion des transform : translateY()

                } else if ($(this).hasClass('load-swipe-t')){
                    var bottom_of_object = $(this).offset().top + $(this).outerHeight() + 500; //Gestion des transform : translateY()

                } else {
                    var bottom_of_object = $(this).offset().top + $(this).outerHeight() - 150;

                }

                var bottom_of_window = $(window).scrollTop() + $(window).height();
                if (bottom_of_window > bottom_of_object) {
                    $(this).removeClass('-init');
                }

            });
        });
    }

    if (ie){
        $('.animg-gristxt').addClass('anim-IE');
    }

    //FIN ANIMATION //




    /*** UTILITIES ***/
    // wait function
    $.wait = function (ms) {
        var defer = $.Deferred();
        setTimeout(function () {
            defer.resolve();
        }, ms);
        return defer;
    };

    //ScrollTo
    function scrollTo(target, condition) {
        if (condition.length) {
            var scroll = target.offset().top - $('header').height(); // prise en compte du header sticky - si sticky partiel, mettre la partie du header qui sera sticky à la place
            $("html, body").stop().animate({scrollTop: scroll}, 1500);
        }
    }

    /*** Fin UTILITIES ***/

    /*** GLOBAL ***/
    // Tooltips trigger
    $('[data-toggle="tooltip"]').tooltip({
        delay: {
            show: 800,
            hide: 50
        }
    });

    // tableau responsive
    $('.wysiwyg table, .main-content table').wrap('<div class="tableau-responsive container"></div>');

    // loader sur les boutons
    $('.btn').on('click', function () {
        if (!$(this).hasClass('no-loader')) {
            if (!$(this).attr('target')) {
                $(this).width($(this).width()); // pour empêcher le changement de taille du bouton
                $(this).addClass('btn-loader');
                $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
            }
            // ajoutez la class "no-loader" aux boutons que vous souhaitez exclure
        }
    });

    // loader sur le bouton submit en input
    $('input[type="submit"]').on('click', function () {
        if ($(this).parents('form')[0].checkValidity()) {
            $(this).wrap('<span class="input-loader"></span>');
            $(this).parent().append('<div class="msg-envoi"><i class="fas fa-circle-notch fa-spin"></i><span>Veuillez patienter...</span></div>');
        }
    });

    // Scroll to error - gravity forms
    scrollTo($(".formulaire-anchor"), $(".gform_validation_error"), 1500); // ajoutez la classe "formulaire-anchor" dans les réglages du formulaire en back-office

    var navhei = $('.conteneur-logo').height();
    var navheix = navhei + 70;
    document.addEventListener('invalid', function (e) {
        $(e.target).addClass("invalid");

        $('html, body').stop().animate({
            scrollTop: ($($(".invalid")[0]).offset().top - navheix)
        }, 300);

        setTimeout(function () {
            $('.invalid').removeClass('invalid');
        }, 300);
    }, true);

    // Tarte au citron contraignant
    setTimeout(function () {
        if ($("#tarteaucitronAlertBig").css("display") === "block") {
            $("#tarteaucitronRoot").addClass("blur");
        }
    }, 1500);
    $document.on("click", "#tarteaucitronPersonalize", function () {
        $("#tarteaucitronRoot").removeClass("blur");
    });
    $document.on("click", "#tarteaucitronClosePanel", function () {
        window.location.reload();
    });


    /*** FIN GLOBAL ***/

    /*** HEADER ***/

    // Si besoin de modifier le header au scroll, décommenter cette section

    // $window.on('scroll', function () {
    //     var windowPos = $window.scrollTop();
    //     if ($window.width() > 992) {
    //         if (windowPos > 10 && !header.hasClass('scrolling')) {
    //             header.addClass('scrolling');
    //         } else if (windowPos <= 10) {
    //             header.removeClass('scrolling');
    //         }
    //     }
    // });
    //
    // $window.on('resize', function () {
    //     if ($window.width() < 992 && header.hasClass('scrolling')) {
    //         header.removeClass('scrolling');
    //     }
    // });

    /*** FIN HEADER ***/


    /*** MEGAMENU ***/


    /** Mégamenu au clic **/
    // niveau1_a.on('click', function(event){
    //     event.preventDefault();
    //     if($(this).parent().hasClass('active')){
    //         niveau1_li.removeClass('active');
    //     } else {
    //         niveau1_li.removeClass('active');
    //         $(this).parent().addClass('active');
    //     }
    // });


    /** Mégamenu au hover **/
    niveau1_a.on('mouseover', function(){
        niveau1_li.removeClass('active');
        $(this).parent().addClass('active');
    });
    $('.menu-desktop').on('mouseleave', function(){
        niveau1_li.removeClass('active');
    });


    /*** FIN MEGAMENU ***/

    /*** MENU BURGER ***/

    // ouverture/fermeture du burger
    $('.hamburger').on('click', function () {
        $(this).toggleClass('is-active');
        header.toggleClass('menu-opened');
        if(burger.hasClass('active')){
            // fermeture du burger
            burger.removeClass('active');
            burger_ul.removeClass('active');
            $('.conteneur-menu-mobile').slideUp();
        } else {
            // ouverture du burger
            burger.addClass('active');
            $('.conteneur-menu-mobile').slideDown();
        }
    });

    $window.on('resize', function(){
       if($window.width() > 992){
           // fermeture du burger
           burger.removeClass('active');
           burger_ul.removeClass('active');
           $('.conteneur-menu-mobile').slideUp();
           $('.hamburger').removeClass('is-active');
           header.removeClass('menu-opened');
        }
    });

    /*** FIN MENU BURGER ***/

    /*** RECHERCHE HEADER ***/

    $('.searchButton').on('click', function(){
        if($(this).hasClass('active')){
            $('#block-searchsolrblock').removeClass('active');
            body.removeClass('no-scroll');
        } else {
            $('#block-searchsolrblock').addClass('active');
            body.addClass('no-scroll');
        }
        $(this).toggleClass('active');
    });

    $('#block-searchsolrblock .btn-close-hub').on('click', function(){
        $('#block-searchsolrblock').removeClass('active');
        $('.searchButton').removeClass('active');
        body.removeClass('no-scroll');
    });


    /*** FIN RECHERCHE HEADER ***/


    /*** SLICKS ****/
    // mettez ici tous les slicks du site

    // slider home
    if($('.home .conteneur-carrousel').length > 0) {
        $('.home .conteneur-carrousel').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            autoplay: 4000,
            speed: 300
        });
    }


    /*** exemple de slickification ***/
    // on load (ne pas oublier le resize)
    // if($(window).width() < 1200){
    //     $('.home .actualites-evenements .conteneur-actus').slick({
    //         dots: true,
    //         arrows: false,
    //         infinite: true,
    //         slidesToScroll: 2,
    //         slidesToShow: 2,
    //         autoplay: 4000,
    //         speed: 300,
    //         responsive: [
    //             {
    //                 breakpoint: 992,
    //                 settings: {
    //                     slidesToScroll: 1,
    //                     slidesToShow: 1,
    //                 }
    //             }
    //         ]
    //     });
    // }
    // if($(window).width() > 1200){
    //     if($('.home .actualites-evenements .conteneur-actus').hasClass('slick-initialized')){
    //         $('.home .actualites-evenements .conteneur-actus').slick('unslick');
    //     }
    // }

    // gestion du resize (ne pas oublier le on load)
    // $(window).on('resize', function(){
    //     if($(window).width() < 1200){
    //         if(!$('.home .actualites-evenements .conteneur-actus').hasClass('slick-initialized')){
    //             $('.home .actualites-evenements .conteneur-actus').slick({
    //                 dots: true,
    //                 arrows: false,
    //                 infinite: true,
    //                 slidesToScroll: 2,
    //                 slidesToShow: 2,
    //                 autoplay: 4000,
    //                 speed: 300,
    //                 responsive: [
    //                     {
    //                         breakpoint: 992,
    //                         settings: {
    //                             slidesToScroll: 1,
    //                             slidesToShow: 1,
    //                         }
    //                     }
    //                 ]
    //             });
    //         }
    //     }
    //
    //     if($(window).width() > 1200){
    //         if(($('.home .actualites-evenements .conteneur-actus')).hasClass('slick-initialized')){
    //             $('.home .actualites-evenements .conteneur-actus').slick('unslick');
    //         }
    //     }
    // });

    /*** FIN SLICKS ***/

    /*** SELECT 2 ***/

    // Exemple d'initialisation de select2 avec placeholder
    // $('#id-du-select').select2({
    //     placeholder: "Le placeholder",
    // });

    /*** FIN SELECT 2 ***/

});